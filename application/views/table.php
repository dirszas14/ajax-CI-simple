<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Test Back-End</title>
      <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css')?>">
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/DataTables/DataTables-1.10.18/css/dataTables.bootstrap4.css')?>"/>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
      <style>
         input.error {
         border: 1px solid red;
         }
         label.error {
         font-weight: normal;
         color: red;
         }
      </style>
   </head>
   <body>
      <div class="container" style="padding-top:100px">
         <table id="table_crud" class="table text-center">
            <thead>
               <tr>
                  <th>Nama</th>
                  <th>Email</th>
               </tr>
            </thead>
         </table>
         <button class="btn btn-success float-right" data-toggle="modal" data-target="#exampleModal" style="margin-top:10px">ADD</button>
      </div>
      <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Create User</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <form action="" id="create_user">
                     <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control" name="nama" id="nama">
                     </div>
                     <div class="form-group">
                        <label>No.Hp</label>
                        <input type="number" class="form-control" name="nohp" id="nohp">
                     </div>
                     <div class="form-group">
                        <label for="">Email</label>
                        <input type="email" class="form-control" name="email" id="email">
                     </div>
                     <div class="form-group">
                        <label for="">Alamat</label>
                        <input type="text" class="form-control" name="alamat" id="alamat">
                        <a style="margin:10px 0; color:white;" class="btn btn-success tambah"><i class="fas fa-plus"></i></a>  
                        <input type="text" class="form-control" name="alamat2" id="alamat2">
                        <a href="#" id="hapus" class="btn btn-danger">Hapus</a>     
                     </div>
               </div>
               <div class="modal-footer">
               <button type="button" class="btn btn-danger" data-dismiss="modal">Back</button>
               <input type="submit" id="btn-simpan" class="btn btn-primary" value="Create">
               </div>
               </form>
            </div>
         </div>
      </div>
   </body>
   <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
   <script src="<?php echo base_url('assets/js/jqueryvalidation.min.js')?>"></script>
   <script src="<?php echo base_url('assets/js/bootstrap.js')?>"></script>
   <script type="text/javascript" src="<?php echo base_url('assets/DataTables/datatables.min.js')?>"></script>
   <script src="<?php echo base_url('assets/Datatables/DataTables-1.10.18/js/dataTables.bootstrap4.js')?>"></script>
   <script src="<?php echo base_url('assets/sweetalert.js')?>"></script>
   <script>
      var alamat2=$('#alamat2');
      var hapus=$('#hapus');
      alamat2.hide();
      hapus.hide();
       $('.tambah').click(function(){
           alamat2.show(); 
           hapus.show();
       })
       $('#hapus').click(function(){
           alamat2.hide();
           hapus.hide();
       })
      
      //////
      var table= $('#table_crud').DataTable({
          "ajax":"<?php echo site_url('table/fetchdata')?>",
          "columns":[
              {"data":"nama"},
              {"data":"email"}
              ]
          
      });
      // $(document).ready(function() {
      //     var max_fields      = 3; //maximum input boxes allowed
      //     var wrapper         = $(".input-wrap"); //Fields wrapper
      //     var add_button      = $(".tambah"); //Add button ID
          
      //     var x = 1; //initlal text box count
      //     $(add_button).click(function(e){ //on add input button click
      //         e.preventDefault();
      //         if(x < max_fields){ //max input box allowed
      //             x++; //text box increment
      //             $(wrapper).append(`<div><input type="text" class="form-control" name="alamat[]"><a href="#" class="remove_field btn btn-danger">Remove</a></div>`); //add input box
      //         }
      //     });
          
      //     $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
      //         e.preventDefault(); $(this).parent('div').remove(); x--;
      //     })
      // });
      //  //Save product
       $('#create_user').on('submit',function(e){
           e.preventDefault();
           $("#create_user").validate({
                         rules:{
                             nama:{
                                 required:true,
                             },
                             nohp:{
                                 required:true,
                                 minlength:12
                             },
                             email:{
                                 required:true,
                                 email:true
                             },
                          alamat:{
                              required:true,
                              minlength:5
                          },
                          alamat2:{
                              required:true,
                              minlength:5
                          }
                         },
                         messages:{
                             nama:"Masukkan Nama Anda",
                             nohp:{
                                 required:"Masukkan Nomor Handphone Anda"
                             },
                             email:"masukkan Email Anda",
                             alamat:"masukkan alamat anda",
                             alamat2:"masukkan alamat anda"
                         },  
                         submitHandler: function (form) {
                          $.ajax({
                      type : "POST",
                      url  : "<?php echo site_url('table/savedata')?>",
                      //dataType : "JSON",
                      data : $('#create_user').serialize(),
                      success: function(data){
                          swal(
                      'Good job!',
                      'Data Berhasil Masuk!',
                      'success'
                      )
                          $('#exampleModal').modal('hide');
                          table.ajax.reload();
                      },
                      
                      
                  });
                  return false;  }
                      });
                      
                 
              });
             
   </script>
</html>