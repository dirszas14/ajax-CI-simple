<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Table extends CI_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('crud_model');
        
    }

    public function index()
    {
        $this->load->view('table');
        
    }
    public function fetchdata(){
        
        $data['data']= $this->crud_model->fetchdata();
        echo json_encode($data);
    }
    
    public function savedata(){
       $data['data']=$this->crud_model->savedata();
       echo json_encode($data);
    }
    
    public function update(){

    }

    public function delete(){

    }

}

/* End of file Table.php */
?>