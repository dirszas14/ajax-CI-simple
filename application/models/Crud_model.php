<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_model extends CI_Model {

    public function fetchdata(){
        $query=$this->db->query('SELECT * from tbl_user');
        return $query->result();
    }

    public function savedata(){
        $data=array(
            'nama'=>$this->input->post('nama'),
            'nohp'=>$this->input->post('nohp'),
            'email'=>$this->input->post('email'),
            'alamat'=>$this->input->post('alamat'),
            'alamat2'=>$this->input->post('alamat2')
        );
        $query =$this->db->insert('tbl_user',$data);
        return $query;
    }

}

/* End of file Crud_model.php */
?>